# Example repository for the Tropical Land Use Group away day
## 2017_12_07

The repo contains this `README.md` and a script to make a pretty picture in R using `ggplot2`. 

Fork the repo to your own Bitbucket account then experiment with pulling, committing, pushing, merging, branching etc.

